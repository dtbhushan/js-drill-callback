const callback1 = require("./callback1")
const callback2 = require("./callback2")
const callback3 = require("./callback3")

function boardData(boardId, boards) {
    for (board of boards) {
        if (board[boardId] = "mcu453ed") {
            return board
        }
    }
}

function listData(boardId, list) {
    for (id in list) {
        if (id == boardId) {
            return list[id]
        }
    }
}

function cardData(listId, lists){
    for (key in lists){
        if (key == listId){
            return lists[key]
        }
    }
}

callback1("mcu453ed", boardData)
callback2("mcu453ed", listData)
callback3("qwsa221", cardData)
callback3("jwkh245", cardData)